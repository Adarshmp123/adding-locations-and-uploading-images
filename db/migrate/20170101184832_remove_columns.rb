class RemoveColumns < ActiveRecord::Migration[5.0]
  def change
    remove_column :users, :index
    remove_column :users, :create
    remove_column :users, :new
  end
end
