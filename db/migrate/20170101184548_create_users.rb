class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :avatar
      t.string :location
      t.string :index
      t.string :new
      t.string :create

      t.timestamps
    end
  end
end
