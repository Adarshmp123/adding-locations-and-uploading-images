json.extract! user, :id, :name, :avatar, :location, :index, :new, :create, :created_at, :updated_at
json.url user_url(user, format: :json)