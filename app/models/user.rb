class User < ApplicationRecord
  mount_uploader :avatar, AvatarUploader
  geocoded_by :location
  after_validation :geocode 

  validates_integrity_of  :avatar
  validates_processing_of :avatar
end
